package com.mobaxe.hillcraftracer.gameobjects;

import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.physics.box2d.joints.WheelJoint;
import com.badlogic.gdx.physics.box2d.joints.WheelJointDef;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.mobaxe.hillcraftracer.HillCraftRacer;
import com.mobaxe.hillcraftracer.helpers.AssetsLoader;
import com.mobaxe.hillcraftracer.screens.GameScreen;
import com.mobaxe.hillcraftracer.ui.VehicleBrakeButton;
import com.mobaxe.hillcraftracer.ui.VehicleGasButton;
import com.mobaxe.hillcraftracer.utils.Constants;

public class Vehicle extends InputAdapter {

	private Body chassis, leftWheel, rightWheel;
	private static WheelJoint leftAxis;
	private static WheelJoint rightAxis;
	private static float motorSpeed;
	private float maxMotorTorque;
	private Sprite carSprite, rwheelSprite, lwheelSprite, brakeSprite, gasSprite;
	private Stage stage;
	private Table table;
	private VehicleGasButton gasButton;
	private VehicleBrakeButton brakeButton;

	private String name;

	public Vehicle(World world, float x, float y, float width, float height, float motorSpeed,
			float maxMotorTorque, float localAnchor) {
		Vehicle.motorSpeed = motorSpeed;
		this.maxMotorTorque = maxMotorTorque;
		BodyDef bodyDef = new BodyDef();
		bodyDef.type = BodyType.DynamicBody;
		bodyDef.position.set(x, y);

		FixtureDef chassisFixtureDef = new FixtureDef(), wheelFixtureDef = new FixtureDef();

		chassisFixtureDef.density = 5f;
		chassisFixtureDef.friction = .4f;
		chassisFixtureDef.restitution = .3f;

		wheelFixtureDef.density = chassisFixtureDef.density * 20f;
		wheelFixtureDef.friction = 20f;
		wheelFixtureDef.restitution = .2f;

		// CHASSIS
		PolygonShape chassisShape = new PolygonShape();
		if (!HillCraftRacer.getSelectedCar().equals(Constants.VEHICLEBUTTON2)) {
			chassisShape.set(new float[] { -width / 1.2f, -height / 5, width / 1.4f, -height / 5,
					width / 1.4f, height / 5, -width / 1.2f, height / 4, });
		} else {
			chassisShape.set(new float[] { -width / 1.6f, -height / 5, width / 1.6f, -height / 5,
					width / 1.6f, height / 5, -width / 1.6f, height / 4, });
		}
		chassisFixtureDef.shape = chassisShape;
		chassis = world.createBody(bodyDef);
		chassis.createFixture(chassisFixtureDef);

		// LEFT WHEEL
		CircleShape lwheelShape = new CircleShape();
		lwheelShape.setRadius(height / 3.5f);
		wheelFixtureDef.shape = lwheelShape;
		leftWheel = world.createBody(bodyDef);
		leftWheel.createFixture(wheelFixtureDef);

		// RIGHT WHEEL
		wheelFixtureDef.density = chassisFixtureDef.density * 6f;
		rightWheel = world.createBody(bodyDef);
		rightWheel.createFixture(wheelFixtureDef);

		// LEFT AXIS
		WheelJointDef axisDef = new WheelJointDef();
		axisDef.bodyA = chassis;
		axisDef.bodyB = leftWheel;
		axisDef.maxMotorTorque = chassisFixtureDef.density * maxMotorTorque;
		axisDef.frequencyHz = chassisFixtureDef.density;
		if (!HillCraftRacer.getSelectedCar().equals(Constants.VEHICLEBUTTON2)) {
			axisDef.localAnchorA.set(-width / 2 * localAnchor + lwheelShape.getRadius(), -height / 2 * 1.44f);
		} else {
			axisDef.localAnchorA.set(-width / 2 * localAnchor + lwheelShape.getRadius(), -height / 2 * 1.11f);
		}
		axisDef.localAxisA.set(Vector2.Y);
		leftAxis = (WheelJoint) world.createJoint(axisDef);

		// RIGHT AXIS
		axisDef.bodyB = rightWheel;
		if (HillCraftRacer.getSelectedCar().equals(Constants.VEHICLEBUTTON2)) {
			axisDef.localAnchorA.x *= -1 + 0.3f;
		} else {
			axisDef.localAnchorA.x *= -1;
		}

		rightAxis = (WheelJoint) world.createJoint(axisDef);

		loadSprites();
		loadSounds();

		createCarStage();

		chassis.setUserData(carSprite);
		leftWheel.setUserData(lwheelSprite);
		rightWheel.setUserData(rwheelSprite);

	}

	private void createCarStage() {

		stage = new Stage(new StretchViewport(Constants.virtualWidth, Constants.virtualHeight));
		table = new Table();

		table.setFillParent(true);
		table.row();

		brakeButton = new VehicleBrakeButton();
		table.add(brakeButton).pad(380, 600, 0, 40);

		gasButton = new VehicleGasButton();
		table.add(gasButton).pad(370, 0, 0, 0);

		stage.addActor(table);
	}

	public void loadSounds() {
	}

	public void loadSprites() {

		if (HillCraftRacer.getSelectedCar().equals(Constants.VEHICLEBUTTON1)) {
			carSprite = new Sprite(AssetsLoader.pickup);
			rwheelSprite = new Sprite(AssetsLoader.wheel);
			lwheelSprite = new Sprite(AssetsLoader.wheel);

			carSprite.setSize(2.7f, 1f);
			carSprite.setOriginCenter();
			rwheelSprite.setSize(0.45f, 0.45f);
			rwheelSprite.setOriginCenter();
			lwheelSprite.setSize(0.45f, 0.45f);
			lwheelSprite.setOriginCenter();

		} else if (HillCraftRacer.getSelectedCar().equals(Constants.VEHICLEBUTTON2)) {
			carSprite = new Sprite(AssetsLoader.beetle);
			rwheelSprite = new Sprite(AssetsLoader.beetleWheel);
			lwheelSprite = new Sprite(AssetsLoader.beetleWheel);

			carSprite.setSize(2.3f, 1f);
			carSprite.setOriginCenter();
			rwheelSprite.setSize(0.45f, 0.45f);
			rwheelSprite.setOriginCenter();
			lwheelSprite.setSize(0.45f, 0.45f);
			lwheelSprite.setOriginCenter();
		} else {
			carSprite = new Sprite(AssetsLoader.monsterTruck);
			rwheelSprite = new Sprite(AssetsLoader.monsterWheel);
			lwheelSprite = new Sprite(AssetsLoader.monsterWheel);

			carSprite.setSize(3.5f, 1.5f);
			carSprite.setOriginCenter();
			rwheelSprite.setSize(1.4f, 1.4f);
			rwheelSprite.setOriginCenter();
			lwheelSprite.setSize(1.4f, 1.4f);
			lwheelSprite.setOriginCenter();
		}
	}

	public void draw(SpriteBatch batch) {
		// CAR IMAGE
		if (chassis.getUserData() != null && chassis.getUserData() instanceof Sprite) {
			Sprite sprite = (Sprite) chassis.getUserData();
			sprite.setPosition(chassis.getPosition().x - sprite.getWidth() / 2f, chassis.getPosition().y
					- sprite.getHeight() / 2);
			sprite.setRotation(chassis.getAngle() * MathUtils.radiansToDegrees);
			sprite.draw(batch);

		}
		// LEFT WHEEL IMAGE
		if (leftWheel.getUserData() != null && leftWheel.getUserData() instanceof Sprite) {
			Sprite sprite = (Sprite) leftWheel.getUserData();
			sprite.setPosition(leftWheel.getPosition().x - sprite.getWidth() / 2f + 0.06f,
					leftWheel.getPosition().y - sprite.getHeight() / 2);
			sprite.rotate(leftAxis.getJointSpeed());
			sprite.draw(batch);
		}
		// RIGHT WHEEL IMAGE
		if (rightWheel.getUserData() != null && rightWheel.getUserData() instanceof Sprite) {
			Sprite sprite = (Sprite) rightWheel.getUserData();
			sprite.setPosition(rightWheel.getPosition().x - sprite.getWidth() / 2f + 0.05f,
					rightWheel.getPosition().y - sprite.getHeight() / 2);
			sprite.rotate(rightAxis.getJointSpeed());
			sprite.draw(batch);
		}

	}

	public static void gas() {
		leftAxis.enableMotor(true);
		leftAxis.setMotorSpeed(-motorSpeed);
		rightAxis.enableMotor(true);
		rightAxis.setMotorSpeed(-motorSpeed);

	}

	public static void brake() {
		leftAxis.enableMotor(true);
		leftAxis.setMotorSpeed(motorSpeed);
		rightAxis.enableMotor(true);
		rightAxis.setMotorSpeed(motorSpeed);
	}

	public static void disableMotor() {
		leftAxis.enableMotor(false);
		rightAxis.enableMotor(false);
	}

	@Override
	public boolean keyDown(int keycode) {
		switch (keycode) {
		case Keys.W:
			leftAxis.enableMotor(true);
			leftAxis.setMotorSpeed(-motorSpeed);
			rightAxis.enableMotor(true);
			rightAxis.setMotorSpeed(-motorSpeed);
			break;

		case Keys.S:
			leftAxis.enableMotor(true);
			leftAxis.setMotorSpeed(motorSpeed);
			rightAxis.enableMotor(true);
			rightAxis.setMotorSpeed(motorSpeed);
			break;
		}
		if (keycode == Keys.BACK) {
			GameScreen.saveScore();
		}
		return true;
	}

	@Override
	public boolean keyUp(int keycode) {
		switch (keycode) {
		case Keys.W:
		case Keys.S:
			leftAxis.enableMotor(false);
			rightAxis.enableMotor(false);
			break;
		}
		return true;
	}

	public Sprite getCarSprite() {
		return carSprite;
	}

	public Sprite getRwheelSprite() {
		return rwheelSprite;
	}

	public Sprite getLwheelSprite() {
		return lwheelSprite;
	}

	public Sprite getBrakeSprite() {
		return brakeSprite;
	}

	public Sprite getGasSprite() {
		return gasSprite;
	}

	public Body getChassis() {
		return chassis;
	}

	public Body getLeftWheel() {
		return leftWheel;
	}

	public Body getRightWheel() {
		return rightWheel;
	}

	public Stage getStage() {
		return stage;
	}

	public float getMaxMotorTorque() {
		return maxMotorTorque;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
