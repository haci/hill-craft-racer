package com.mobaxe.hillcraftracer.ui;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.mobaxe.hillcraftracer.gameobjects.Vehicle;
import com.mobaxe.hillcraftracer.helpers.AssetsLoader;

public class VehicleBrakeButton extends Button {

	public static int clickCounter;

	private String buttonUp;
	private String buttonOver;
	private Skin skin;
	private ButtonStyle style;

	public VehicleBrakeButton() {
		buttonUp =  "ButtonUp";
		buttonOver =  "ButtonOver";
		initSkins();
		setButtonStyle();
		clickListener();
	}

	private void initSkins() {
		skin = new Skin();
		skin.add(buttonUp, AssetsLoader.brake);
		skin.add(buttonOver, AssetsLoader.brakeOn);
	}

	public void setButtonStyle() {
		style = new ButtonStyle();
		style.up = skin.getDrawable(buttonUp);
		style.over = skin.getDrawable(buttonOver);
		style.down = skin.getDrawable(buttonOver);
		setStyle(style);
	}

	private void clickListener() {

		addListener(new InputListener() {
			public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
				clickCounter++;
				Vehicle.brake();
				return true;
			}

			public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
				if (event.getStageX() > getX() && event.getStageX() < getX() + getWidth()
						&& event.getStageY() < getY() + getHeight() && event.getStageY() > getY()) {
					if (clickCounter == 1) {
						buttonFunction(event);
					}
				} else {
					clickCounter--;
				}
			}

			private void buttonFunction(InputEvent event) {

				Vehicle.disableMotor();

				clickCounter = 0;
			}

		});
	}

}
