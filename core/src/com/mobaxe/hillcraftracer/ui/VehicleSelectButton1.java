package com.mobaxe.hillcraftracer.ui;

import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.mobaxe.hillcraftracer.helpers.AssetsLoader;
import com.mobaxe.hillcraftracer.utils.Constants;

public class VehicleSelectButton1 extends Button {

	public static int clickCounter;

	private String buttonUp;
	private String buttonOver;
	private Skin skin;
	private ButtonStyle style;

	private MyRunnable runnable;

	public VehicleSelectButton1() {
		buttonUp = "ButtonUp";
		buttonOver = "ButtonOver";
		runnable = new MyRunnable(Constants.VEHICLEBUTTON1, "");
		initSkins();
		setButtonStyle();
		clickListener();
	}

	private void initSkins() {
		skin = new Skin();
		skin.add(buttonUp, AssetsLoader.car1);
		skin.add(buttonOver, AssetsLoader.car1);
	}

	public void setButtonStyle() {
		style = new ButtonStyle();
		style.up = skin.getDrawable(buttonUp);
		style.over = skin.getDrawable(buttonOver);
		style.down = skin.getDrawable(buttonOver);
		setStyle(style);
	}

	private void clickListener() {

		addListener(new InputListener() {
			public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
				clickCounter++;
				return true;
			}

			public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
				if (event.getStageX() > getX() && event.getStageX() < getX() + getWidth()
						&& event.getStageY() < getY() + getHeight() && event.getStageY() > getY()) {
					if (clickCounter == 1) {
						buttonFunction(event);
					}
				} else {
					clickCounter--;
				}
			}

			private void buttonFunction(InputEvent event) {

				event.getStage().addAction(
						Actions.sequence(Actions.fadeOut(.7f, Interpolation.pow5Out), Actions.run(runnable)));

				clickCounter = 0;
			}

		});
	}

}
