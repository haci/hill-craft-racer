package com.mobaxe.hillcraftracer.ui;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.mobaxe.hillcraftracer.GameState;
import com.mobaxe.hillcraftracer.MyScreens;
import com.mobaxe.hillcraftracer.ScreenManager;
import com.mobaxe.hillcraftracer.helpers.AssetsLoader;
import com.mobaxe.hillcraftracer.screens.GameScreen;

public class HomeButton extends Button {

	public static int clickCounter;

	private String buttonUp;
	private String buttonOver;
	private String buttonName = "Home";
	private Skin skin;
	private ButtonStyle style;

	public HomeButton() {
		buttonUp = buttonName + "ButtonUp";
		buttonOver = buttonName + "ButtonOver";
		initSkins();
		setButtonStyle();
		clickListener(buttonName);
	}

	private void initSkins() {
		skin = new Skin();
		skin.add(buttonUp, AssetsLoader.homeBtn);
		skin.add(buttonOver, AssetsLoader.homeBtn);
	}

	public void setButtonStyle() {
		style = new ButtonStyle();
		style.up = skin.getDrawable(buttonUp);
		style.over = skin.getDrawable(buttonOver);
		style.down = skin.getDrawable(buttonOver);
		setStyle(style);
	}

	private void clickListener(final String buttonName) {

		addListener(new InputListener() {
			public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
				clickCounter++;
				return true;
			}

			public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
				if (event.getStageX() > getX() && event.getStageX() < getX() + getWidth()
						&& event.getStageY() < getY() + getHeight() && event.getStageY() > getY()) {
					if (clickCounter == 1) {
						buttonFunction(event);
					}
				} else {
					clickCounter--;
				}
			}

			private void buttonFunction(InputEvent event) {

				goToHome();

				clickCounter = 0;
			}

		});
	}

	private void goToHome() {
		GameScreen.saveScore();
		ScreenManager.getInstance().dispose(MyScreens.GAME_SCREEN);
		ScreenManager.getInstance().show(MyScreens.VEHICLE_SELECTION_SCREEN);
	}

}
