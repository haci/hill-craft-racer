package com.mobaxe.hillcraftracer.ui;

import com.mobaxe.hillcraftracer.GameState;
import com.mobaxe.hillcraftracer.HillCraftRacer;
import com.mobaxe.hillcraftracer.MyScreens;
import com.mobaxe.hillcraftracer.ScreenManager;
import com.mobaxe.hillcraftracer.screens.GameScreen;

public class MyRunnable implements Runnable {

	private String selectedVehicle;
	private String selectedStage;

	public MyRunnable(String selectedVehicle, String selectedStage) {
		this.selectedVehicle = selectedVehicle;
		this.selectedStage = selectedStage;
	}

	@Override
	public void run() {
		if (selectedStage.isEmpty()) {
			HillCraftRacer.setSelectedCar(selectedVehicle);
			goToStageSelectionScreen();
		}
		if (selectedVehicle.isEmpty()) {
			HillCraftRacer.setSelectedStage(selectedStage);
			goToGameScreen();
		}

	}

	private void goToStageSelectionScreen() {
		ScreenManager.getInstance().dispose(MyScreens.VEHICLE_SELECTION_SCREEN);
		ScreenManager.getInstance().show(MyScreens.STAGE_SELECTION_SCREEN);
	}

	private void goToGameScreen() {
		ScreenManager.getInstance().dispose(MyScreens.STAGE_SELECTION_SCREEN);
		ScreenManager.getInstance().show(MyScreens.GAME_SCREEN);
		changeGameState();
	}

	private void changeGameState() {
		GameScreen.gameState = GameState.PLAYAGAIN;
	}

}
