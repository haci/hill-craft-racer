package com.mobaxe.hillcraftracer.ui;

import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.mobaxe.hillcraftracer.HillCraftRacer;
import com.mobaxe.hillcraftracer.helpers.AssetsLoader;
import com.mobaxe.hillcraftracer.utils.Constants;

public class StageSelectButton3 extends Button {

	public static int clickCounter;

	private String buttonUp;
	private String buttonOver;
	private Skin skin;
	private ButtonStyle style;

	private MyRunnable runnable;

	public StageSelectButton3() {
		buttonUp = "ButtonUp";
		buttonOver = "ButtonOver";
		runnable = new MyRunnable("", Constants.STAGEBUTTON3);
		initSkins();
		setButtonStyle();
		clickListener();
	}

	private void initSkins() {
		skin = new Skin();
		skin.add(buttonUp, AssetsLoader.moon);
		skin.add(buttonOver, AssetsLoader.moon);
	}

	public void setButtonStyle() {
		style = new ButtonStyle();
		style.up = skin.getDrawable(buttonUp);
		style.over = skin.getDrawable(buttonOver);
		style.down = skin.getDrawable(buttonOver);
		setStyle(style);
	}

	private void clickListener() {

		addListener(new InputListener() {
			public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
				clickCounter++;
				return true;
			}

			public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
				if (event.getStageX() > getX() && event.getStageX() < getX() + getWidth()
						&& event.getStageY() < getY() + getHeight() && event.getStageY() > getY()) {
					if (clickCounter == 1) {
						buttonFunction(event);
					}
				} else {
					clickCounter--;
				}
			}

			private void buttonFunction(InputEvent event) {
				if (HillCraftRacer.getPreferences().getS3Lock() == false) {
					event.getListenerActor().addAction(Actions.sequence(Actions.moveBy(15, 0), Actions.moveBy(-15, 0)));
					if (HillCraftRacer.getPreferences().getTotalBalance() >= Constants.STAGE3_COST) {
						HillCraftRacer.getPreferences().saveS3Lock(true);
						HillCraftRacer.getPreferences().saveTotalBalance(
								HillCraftRacer.getPreferences().getTotalBalance() - Constants.STAGE3_COST);
						event.getListenerActor().setColor(1, 1, 1, 1);
					}
				} else {
					event.getStage().addAction(
							Actions.sequence(Actions.fadeOut(.7f, Interpolation.pow5Out), Actions.run(runnable)));
				}
				clickCounter = 0;
			}

		});
	}

}
