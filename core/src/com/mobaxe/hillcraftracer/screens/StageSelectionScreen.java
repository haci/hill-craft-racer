package com.mobaxe.hillcraftracer.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.mobaxe.hillcraftracer.HillCraftRacer;
import com.mobaxe.hillcraftracer.helpers.AssetsLoader;
import com.mobaxe.hillcraftracer.ui.StageSelectButton1;
import com.mobaxe.hillcraftracer.ui.StageSelectButton2;
import com.mobaxe.hillcraftracer.ui.StageSelectButton3;
import com.mobaxe.hillcraftracer.utils.Constants;

public class StageSelectionScreen implements Screen {

	private Stage stage;
	private Table bgTable;
	private Table lblTable;
	private Table vbTable;
	private Texture bgTexture;
	private Sprite bgSprite;
	private Image bgImage;
	private Label coinsLabel;
	private LabelStyle labelStyle;

	private StageSelectButton1 stage1;
	private StageSelectButton2 stage2;
	private StageSelectButton3 stage3;

	public StageSelectionScreen() {

		stage = new Stage(new StretchViewport(Constants.virtualWidth, Constants.virtualHeight));
		bgTable = new Table();
		lblTable = new Table();
		vbTable = new Table();
		labelStyle = new LabelStyle();
		labelStyle.font = AssetsLoader.scoreText;
		coinsLabel = new Label(String.valueOf(HillCraftRacer.getPreferences().getTotalBalance()), labelStyle);
		lblTable.add(coinsLabel).pad(0, 200, 850, 0);

	}

	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		stage.act(delta);
		stage.draw();
	}

	@Override
	public void resize(int width, int height) {
		stage.getViewport().update(width, height, true);
	}

	@Override
	public void show() {
		Gdx.input.setInputProcessor(stage);
		setBackgroundImage();
		createButtons();
	}

	private void createButtons() {

		bgTable.setFillParent(true);

		stage1 = new StageSelectButton1();
		stage1.setName("s1");
		vbTable.add(stage1).pad(0, 820, 450, 0);

		stage2 = new StageSelectButton2();
		stage2.setName("s2");
		if (HillCraftRacer.getPreferences().getS2Lock() == false) {
			stage2.setColor(0.5f, 0.5f, 0.5f, 0.8f);
		}
		vbTable.add(stage2).pad(0, 0, 450, 0);

		stage3 = new StageSelectButton3();
		stage3.setName("s3");
		if (HillCraftRacer.getPreferences().getS3Lock() == false) {
			stage3.setColor(0.5f, 0.5f, 0.5f, 0.8f);
		}
		vbTable.add(stage3).pad(0, 0, 450, 0);

		bgImage = new Image(bgTexture);
		bgTable.add(bgImage);

		stage.addActor(bgTable);
		stage.addActor(lblTable);
		stage.addActor(vbTable);

	}

	private void setBackgroundImage() {
		bgTexture = AssetsLoader.stageselectbg;

		bgSprite = new Sprite(bgTexture);
		bgSprite.setColor(0, 0, 0, 0);
		bgSprite.setX(Constants.virtualWidth - bgSprite.getWidth() / 2);
		bgSprite.setY(Constants.virtualHeight - bgSprite.getHeight() / 2);
	}

	@Override
	public void hide() {
	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {
	}

	@Override
	public void dispose() {
	}

}
