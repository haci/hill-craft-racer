package com.mobaxe.hillcraftracer.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.mobaxe.hillcraftracer.HillCraftRacer;
import com.mobaxe.hillcraftracer.helpers.AssetsLoader;
import com.mobaxe.hillcraftracer.ui.VehicleSelectButton1;
import com.mobaxe.hillcraftracer.ui.VehicleSelectButton2;
import com.mobaxe.hillcraftracer.ui.VehicleSelectButton3;
import com.mobaxe.hillcraftracer.utils.Constants;

public class VehicleSelectionScreen implements Screen {

	private Stage stage;
	private Table bgTable;
	private Table lblTable;
	private Table vbTable;
	private Texture bgTexture;
	private Sprite bgSprite;
	private Image bgImage;
	private Label coinsLabel;
	private LabelStyle labelStyle;

	private VehicleSelectButton1 vehicle1;
	private VehicleSelectButton2 vehicle2;
	private VehicleSelectButton3 vehicle3;

	public VehicleSelectionScreen() {
		stage = new Stage(new StretchViewport(Constants.virtualWidth, Constants.virtualHeight));
		bgTable = new Table();
		lblTable = new Table();
		vbTable = new Table();
		labelStyle = new LabelStyle();
		labelStyle.font = AssetsLoader.scoreText;
		coinsLabel = new Label(String.valueOf(HillCraftRacer.getPreferences().getTotalBalance()), labelStyle);
		lblTable.add(coinsLabel).pad(0, 200, 850, 0);

	}

	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		stage.act(delta);
		stage.draw();

	}

	@Override
	public void resize(int width, int height) {
		stage.getViewport().update(width, height, true);

	}

	@Override
	public void show() {
		Gdx.input.setInputProcessor(stage);
		setBackgroundImage();
		createButtons();
	}

	private void createButtons() {
		bgTable.setFillParent(true);

		vehicle1 = new VehicleSelectButton1();
		vehicle1.setName("v1");
		vbTable.add(vehicle1).pad(0, 820, 450, 0);

		vehicle2 = new VehicleSelectButton2();
		vehicle2.setName("v2");
		if (HillCraftRacer.getPreferences().getV2Lock() == false) {
			vehicle2.setColor(0.5f, 0.5f, 0.5f, 0.8f);
		}
		vbTable.add(vehicle2).pad(0, 0, 450, 0);

		vehicle3 = new VehicleSelectButton3();
		vehicle3.setName("v3");
		if (HillCraftRacer.getPreferences().getV3Lock() == false) {
			vehicle3.setColor(0.5f, 0.5f, 0.5f, 0.8f);
		}
		vbTable.add(vehicle3).pad(0, 0, 450, 0);

		bgImage = new Image(bgTexture);
		bgTable.add(bgImage);

		stage.addActor(bgTable);
		stage.addActor(lblTable);
		stage.addActor(vbTable);

	}

	private void setBackgroundImage() {
		bgTexture = AssetsLoader.vehicleselectbg;

		bgSprite = new Sprite(bgTexture);
		bgSprite.setColor(0, 0, 0, 0);
		bgSprite.setX(Constants.virtualWidth - bgSprite.getWidth() / 2);
		bgSprite.setY(Constants.virtualHeight - bgSprite.getHeight() / 2);
	}

	@Override
	public void hide() {
	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {
	}

	@Override
	public void dispose() {
	}

}
