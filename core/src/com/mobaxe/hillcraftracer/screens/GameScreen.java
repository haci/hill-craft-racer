package com.mobaxe.hillcraftracer.screens;

import java.util.Random;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.Texture.TextureWrap;
import com.badlogic.gdx.graphics.g2d.PolygonRegion;
import com.badlogic.gdx.graphics.g2d.PolygonSpriteBatch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.EarClippingTriangulator;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.Manifold;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Array;
import com.mobaxe.hillcraftracer.ActionResolver;
import com.mobaxe.hillcraftracer.GameState;
import com.mobaxe.hillcraftracer.HillCraftRacer;
import com.mobaxe.hillcraftracer.gameobjects.Vehicle;
import com.mobaxe.hillcraftracer.helpers.AssetsLoader;
import com.mobaxe.hillcraftracer.helpers.Box2DFactory;
import com.mobaxe.hillcraftracer.helpers.CustomColor;
import com.mobaxe.hillcraftracer.helpers.LinearGradientBackground;
import com.mobaxe.hillcraftracer.ui.HomeButton;
import com.mobaxe.hillcraftracer.ui.PlayAgainButton;
import com.mobaxe.hillcraftracer.utils.Constants;

public class GameScreen extends InputAdapter implements Screen {

	private SpriteBatch batch;
	private World world;
	@SuppressWarnings("unused")
	private Box2DDebugRenderer debugRenderer;
	private OrthographicCamera camera;
	private Sprite cloudS;
	private Sprite coinS;
	public static GameState gameState = GameState.RUNNING;
	private Matrix4 normalProjection;
	private SpriteBatch scoreBatcher;
	private PlayAgainButton playAgain;
	private HomeButton homeButton;
	private Table table;
	private static ActionResolver actionResolver;
	private static int adCounter = Constants.INTERSTITIAL_FREQ - 1;
	private static boolean adIsShowed = false;
	private InputMultiplexer multiplexer;
	private Vehicle vehicle;
	private Body screenBlock;
	private Body cloudBody;
	private Body coinBody;
	private Array<Body> polygons = new Array<Body>();
	private Array<Body> tmpBodies = new Array<Body>();
	private Array<Body> clouds = new Array<Body>();
	private Array<Body> coinsToDestroy = new Array<Body>();
	private Vector2 startVertices = new Vector2();
	private Vector2 endVertices = new Vector2();
	private Random rnd = new Random();
	private PolygonShape shape;
	private int vertexCount;
	private float[] vertices;
	private Vector2 mTmp = new Vector2();
	private TextureRegion terrainTextureRegion;
	private PolygonSpriteBatch pBatch = new PolygonSpriteBatch();
	private Array<PolygonRegion> pRegions = new Array<PolygonRegion>();
	private EarClippingTriangulator triangulator;
	private LinearGradientBackground linearBg = new LinearGradientBackground();
	private static int score;
	private float runTime;
	private Vector2 gravity = new Vector2(0, -5);

	public GameScreen(ActionResolver actionResolver) {
		GameScreen.actionResolver = actionResolver;
	}

	@Override
	public void show() {
		// CAMERA STUFF
		camera = new OrthographicCamera(Constants.widthMeters, Constants.heightMeters);
		camera.zoom = 2;
		// DEBUG RENDERER
		debugRenderer = new Box2DDebugRenderer();

		// TEXT STUFF (SCORE BATCH)
		normalProjection = new Matrix4()
				.setToOrtho2D(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		scoreBatcher = new SpriteBatch();
		scoreBatcher.setProjectionMatrix(normalProjection);

		// CREATING TERRAIN TEXTURE AND SETTINGS
		if (HillCraftRacer.getSelectedStage().equals(Constants.STAGEBUTTON1)) {
			terrainTextureRegion = new TextureRegion(AssetsLoader.grass);
			linearBg.setGradient(CustomColor.DARK_BLUE_GRASS, CustomColor.LIGHT_BLUE_GRASS, false);
		} else if (HillCraftRacer.getSelectedStage().equals(Constants.STAGEBUTTON2)) {
			terrainTextureRegion = new TextureRegion(AssetsLoader.sand);
			linearBg.setGradient(CustomColor.LIGHT_BLUE_DESERT, CustomColor.LIGHT_YELLOW_DESERT, false);
		} else {
			terrainTextureRegion = new TextureRegion(AssetsLoader.gray);
			linearBg.setGradient(CustomColor.DARK_BLACK_MOON, CustomColor.LIGHT_BLACK_MOON, false);
			gravity.set(0, -1.8f);
		}

		terrainTextureRegion.getTexture().setWrap(TextureWrap.ClampToEdge, TextureWrap.ClampToEdge);
		terrainTextureRegion.getTexture().setFilter(TextureFilter.Linear, TextureFilter.Linear);

		// WORLD STUFF
		createWorld();
		createWorldContactListener();

		// VEHICLE STUFF
		if (HillCraftRacer.getSelectedCar().equals(Constants.VEHICLEBUTTON1)) {
			vehicle = new Vehicle(world, camera.viewportWidth + 2.3f, camera.viewportHeight + 3, 1.5f, 0.7f,
					30, 3, 1.25f);
		} else if (HillCraftRacer.getSelectedCar().equals(Constants.VEHICLEBUTTON2)) {
			vehicle = new Vehicle(world, camera.viewportWidth + 2.3f, camera.viewportHeight + 3, 1.5f, 0.7f,
					35, 5, 1.5f);
		} else {
			vehicle = new Vehicle(world, camera.viewportWidth + 2.3f, camera.viewportHeight + 3, 2.1f, 1.1f,
					40, 15, 1.55f);
		}

		// HANDLING INPUT WITH MULTIPLEXER
		multiplexer = new InputMultiplexer();
		multiplexer.addProcessor(this);
		multiplexer.addProcessor(vehicle);
		multiplexer.addProcessor(vehicle.getStage());
		Gdx.input.setInputProcessor(multiplexer);

		// CREATING TRIANGULATOR
		triangulator = new EarClippingTriangulator();

		// CLOUD SPRITES AND SETTINGS
		cloudS = new Sprite(AssetsLoader.cloud);
		cloudS.getTexture().setFilter(TextureFilter.Linear, TextureFilter.Linear);
		cloudS.setSize(2f, 1.1f);
		cloudS.setOriginCenter();

		// COIN SPRITES AND SETTINGS
		coinS = new Sprite(AssetsLoader.coin);
		coinS.getTexture().setFilter(TextureFilter.Linear, TextureFilter.Linear);
		coinS.setSize(0.6f, 0.6f);
		coinS.setOriginCenter();

		// CREATING GRADIENT COLOR AND GROUND
		createGround();

		// CREATING TABLE & BATCH & BUTTONS
		refreshButton();
		batch = new SpriteBatch();

	}

	public void texturingPolygons() {

		if (polygons.size >= 1) {
			shape = (PolygonShape) polygons.peek().getFixtureList().first().getShape();
			vertexCount = shape.getVertexCount();
			vertices = new float[vertexCount * 2];
			for (int k = 0; k < vertexCount; k++) {
				shape.getVertex(k, mTmp);
				mTmp.rotate(polygons.peek().getAngle() * MathUtils.radiansToDegrees);
				mTmp.add(polygons.peek().getPosition());
				vertices[k * 2] = mTmp.x;
				vertices[k * 2 + 1] = mTmp.y;
			}

			short triangles[] = triangulator.computeTriangles(vertices).toArray();

			pRegions.add(new PolygonRegion(terrainTextureRegion, vertices, triangles));
		}
	}

	@Override
	public void render(float delta) {
		linearBg.draw();
//		debugRenderer.render(world, camera.combined);

		switch (gameState) {
		case RUNNING:
			gameRunning(delta);
			break;
		case PLAYAGAIN:
			playAgain();
			break;
		}

	}

	public void drawTerrain() {

		if (camera.position.x + 10 > endVertices.x) {
			texturingPolygons();
			updateRoad();
			if (HillCraftRacer.getSelectedStage() != Constants.STAGEBUTTON3) {
				updateClouds();
			}
			updateCoins();
		}

		// DESTROY COINS WHICH ARE COLLIDED
		if (coinsToDestroy.size != 0) {
			for (int i = 0; i < coinsToDestroy.size; i++) {
				world.destroyBody(coinsToDestroy.removeIndex(i));
			}
			coinsToDestroy.clear();
		}

		// DESTROY COINS WHICH ARE NOT COLLIDED
		Array<Body> tmpBodies = new Array<Body>();
		world.getBodies(tmpBodies);
		for (Body body : tmpBodies) {
			if (body.getUserData() != null && body.getUserData().toString().equals(coinS.toString())) {
				if (body.getPosition().x + 20 < camera.position.x) {
					world.destroyBody(body);
				}
			}
		}

		pBatch.setProjectionMatrix(camera.combined);
		pBatch.begin();
		for (int i = 0; i < pRegions.size; i++) {

			pBatch.draw(pRegions.get(i), 0, 0, terrainTextureRegion.getTexture().getWidth(),
					terrainTextureRegion.getTexture().getHeight());

		}
		pBatch.end();

	}

	private void updateCoins() {

		if (runTime > 5 && runTime < 30) {
			float x = 0.01f;
			float y = 0.5f;
			coinBody = Box2DFactory.createCoins(world, endVertices.x + x, endVertices.y + y);
			coinBody.setUserData(coinS);
			coinBody.getFixtureList().first().setSensor(true);
		} else if (runTime > 30 && runTime < 40) {
		} else if (runTime > 40 && runTime < 60) {
			float x = 0.01f;
			float y = 0.5f;
			coinBody = Box2DFactory.createCoins(world, endVertices.x + x, endVertices.y + y);
			coinBody.setUserData(coinS);
			coinBody.getFixtureList().first().setSensor(true);
		} else if (runTime > 60 && runTime < 70) {
		} else if (runTime > 70 && runTime < 85) {
			float x = 0.01f;
			float y = 0.5f;
			coinBody = Box2DFactory.createCoins(world, endVertices.x + x, endVertices.y + y);
			coinBody.setUserData(coinS);
			coinBody.getFixtureList().first().setSensor(true);
			runTime = 0;
		}

	}

	private void playAgain() {

		saveScore();

		score = HillCraftRacer.getPreferences().getTotalBalance();

		// RESTART AD
		adIsShowed = false;

		// CLEAR
		runTime = 0;
		pRegions.clear();
		polygons.clear();
		clouds.clear();
		coinsToDestroy.clear();

		// CHANGE GAME STATE
		gameState = GameState.RUNNING;

	}

	private void gameRunning(float delta) {

		runTime += delta;

		batch.setProjectionMatrix(camera.combined);
		batch.begin();
		drawGameObjects();
		batch.end();

		world.step(1f / 60f, 6, 2);

		drawScore();
		drawTerrain();

		vehicle.getStage().act(delta);
		vehicle.getStage().draw();

	}

	public static void showIntersititial() {

		// Ads every X refresh!
		if (adCounter == Constants.INTERSTITIAL_FREQ) {
			if (!adIsShowed) {
				actionResolver.showOrLoadInterstital();
				adIsShowed = true;
				adCounter--;
			}
		} else if (adCounter == 0) {
			adCounter = Constants.INTERSTITIAL_FREQ;
		} else {
			adCounter--;
		}

	}

	public void refreshButton() {
		table = new Table();
		table.setFillParent(true);

		playAgain = new PlayAgainButton();
		table.add(playAgain).pad(0, 720, 400, 20);

		homeButton = new HomeButton();
		table.add(homeButton).pad(0, 0, 400, 40);

		vehicle.getStage().addActor(table);

	}

	private void drawScore() {

		scoreBatcher.setProjectionMatrix(normalProjection);
		scoreBatcher.begin();
		AssetsLoader.scoreText.draw(scoreBatcher, "" + getScore(),
				Gdx.graphics.getWidth() - Gdx.graphics.getWidth() + 35, Gdx.graphics.getHeight() - 35f);
		scoreBatcher.end();

	}

	private void drawGameObjects() {

		// IMAGES
		world.getBodies(tmpBodies);
		for (Body body : tmpBodies) {
			if (body.getUserData() != null && body.getUserData() instanceof Sprite) {
				if (!body.getUserData().toString().equals(vehicle.getCarSprite().toString())
						&& !body.getUserData().toString().equals(vehicle.getLwheelSprite().toString())
						&& !body.getUserData().toString().equals(vehicle.getRwheelSprite().toString())) {

					if (body.getUserData().toString().equals(cloudS.toString())) {
						Sprite sprite = (Sprite) body.getUserData();
						sprite.setPosition(body.getPosition().x, body.getPosition().y - 1);
						sprite.draw(batch);
					} else {
						Sprite sprite = (Sprite) body.getUserData();
						sprite.setPosition(body.getPosition().x - sprite.getWidth() / 2, body.getPosition().y
								- sprite.getHeight() / 2);
						sprite.draw(batch);
					}
				}
			}
		}

		// VEHICLE
		if (vehicle.getChassis().getPosition().x > screenBlock.getPosition().x + 2.6f) {
			camera.position.set(vehicle.getChassis().getPosition().x + 2,
					vehicle.getChassis().getPosition().y, 0);
			camera.update();
		}
		vehicle.draw(batch);

	}

	@Override
	public void resize(int width, int height) {
		vehicle.getStage().getViewport().update(width, height, true);
	}

	private void createWorldContactListener() {
		world.setContactListener(new ContactListener() {

			@Override
			public void preSolve(Contact contact, Manifold oldManifold) {
			}

			@Override
			public void postSolve(Contact contact, ContactImpulse impulse) {
			}

			@Override
			public void endContact(Contact contact) {
			}

			@Override
			public void beginContact(Contact contact) {
				Body bodyA = contact.getFixtureA().getBody();
				Boolean isSensor = contact.getFixtureB().isSensor();
				if (bodyA.equals(vehicle.getRightWheel()) || bodyA.equals(vehicle.getLeftWheel()) && isSensor) {
					incScore(10);
					coinsToDestroy.add(contact.getFixtureB().getBody());
				}

			}
		});
	}

	private void incScore(int increment) {
		score += increment;
	}

	private static int getScore() {
		return score;
	}

	public static void saveScore() {
		if (getScore() != 0) {
			HillCraftRacer.getPreferences().saveTotalBalance(getScore());
		} else {
			score = HillCraftRacer.getPreferences().getTotalBalance();
		}
	}

	private void updateClouds() {

		float x = rnd.nextFloat() * 1.1f;
		float y = rnd.nextInt(1) + 1;

		if (clouds.size < 4) {
			if (clouds.size != 0) {
				cloudBody = Box2DFactory.createCloud(world, clouds.peek().getPosition().x + x,
						camera.position.y + y);
			} else {
				cloudBody = Box2DFactory.createCloud(world, 4, 3);
			}
			cloudBody.setUserData(cloudS);
			clouds.add(cloudBody);
		}

		if (clouds.get(0).getPosition().x + 20 < vehicle.getChassis().getPosition().x) {
			world.destroyBody(clouds.removeIndex(0));
		}

	}

	private void updateRoad() {

		float val = rnd.nextFloat();
		float width = rnd.nextInt(5) + 2;
		float hill = startVertices.y + val * MathUtils.cos(endVertices.x / 200 * MathUtils.PI2);

		startVertices.set(endVertices.x, endVertices.y);
		endVertices.set(endVertices.x + width, endVertices.y - hill);

		// POLYGON
		Vector2[] vertices = { startVertices, endVertices, new Vector2(startVertices.x, -15),
				new Vector2(endVertices.x, -15), };

		polygons.add(Box2DFactory.createPolygon(world, vertices));

		// REMOVE BODIES IN THE SCREEN'S LEFT and CREATE NEW BLOCK
		if (camera.position.x > screenBlock.getPosition().x + 35) {
			world.destroyBody(screenBlock);
			if (polygons.size >= 1) {
				world.destroyBody(polygons.removeIndex(0));
			}
			pRegions.removeIndex(0);
			screenBlock = Box2DFactory.createScreenBlock(world, camera.position.x - 19);
		}

	}

	public void createGround() {

		startVertices.set(4.9f, -2);
		endVertices.set(-4.9f, -2);

		screenBlock = Box2DFactory.createScreenBlock(world, endVertices.x);

	}

	private void createWorld() {
		world = new World(gravity, true);
	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub

	}

	@Override
	public void pause() {

	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub

	}

	@Override
	public void dispose() {
		linearBg.dispose();

	}

}
