package com.mobaxe.hillcraftracer;

import com.badlogic.gdx.Game;
import com.mobaxe.hillcraftracer.helpers.AssetsLoader;
import com.mobaxe.hillcraftracer.helpers.HillCraftPreferences;

public class HillCraftRacer extends Game {

	public static ActionResolver actionResolver;
	private static String selectedCar;
	private static String selectedStage;
	private static HillCraftPreferences hcrPrefs;

	public HillCraftRacer(ActionResolver actionResolver) {
		HillCraftRacer.actionResolver = actionResolver;
	}

	@Override
	public void create() {
		hcrPrefs = new HillCraftPreferences();
		AssetsLoader.loadTexturesOnCreate();
		ScreenManager.getInstance().initialize(this);
		ScreenManager.getInstance().show(MyScreens.SPLASH_SCREEN);
	}

	@Override
	public void dispose() {
		ScreenManager.getInstance().dispose();
		AssetsLoader.dispose();
	}

	public static void setSelectedCar(String car) {
		selectedCar = car;
	}

	public static void setSelectedStage(String stage) {
		selectedStage = stage;
	}

	public static String getSelectedCar() {
		return selectedCar;
	}

	public static String getSelectedStage() {
		return selectedStage;
	}

	public static HillCraftPreferences getPreferences() {
		return hcrPrefs;
	}

}