package com.mobaxe.hillcraftracer;

import com.badlogic.gdx.Screen;
import com.mobaxe.hillcraftracer.screens.GameScreen;
import com.mobaxe.hillcraftracer.screens.SplashScreen;
import com.mobaxe.hillcraftracer.screens.StageSelectionScreen;
import com.mobaxe.hillcraftracer.screens.VehicleSelectionScreen;

public enum MyScreens {

	SPLASH_SCREEN {
		public Screen getScreenInstance() {
			return new SplashScreen();
		}
	},
	VEHICLE_SELECTION_SCREEN {
		public Screen getScreenInstance() {
			return new VehicleSelectionScreen();
		}
	},
	STAGE_SELECTION_SCREEN {
		public Screen getScreenInstance() {
			return new StageSelectionScreen();
		}
	},
	GAME_SCREEN {
		public Screen getScreenInstance() {
			return new GameScreen(HillCraftRacer.actionResolver);
		}
	};
	public abstract Screen getScreenInstance();

}
