package com.mobaxe.hillcraftracer.helpers;

import com.badlogic.gdx.graphics.Color;

public class CustomColor {

	public static final Color DARK_BLUE_GRASS = new Color(0, 0, 0.5f, 1);
	public static final Color LIGHT_BLUE_GRASS = new Color(0, 0.6f, 0.7f, 1);
	public static final Color DARK_BLACK_MOON = new Color(0, 0, 0, 1);
	public static final Color LIGHT_BLACK_MOON = new Color(0.3f, 0.3f, 0.3f, 1);
	public static final Color LIGHT_BLUE_DESERT = new Color(0, 0.6f, 0.9f, 1);
	public static final Color LIGHT_YELLOW_DESERT = new Color(0.6f, 0.6f, 0.2f, 1);

}
