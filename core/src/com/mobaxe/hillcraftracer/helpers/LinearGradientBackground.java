package com.mobaxe.hillcraftracer.helpers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Mesh;
import com.badlogic.gdx.graphics.VertexAttribute;
import com.badlogic.gdx.graphics.VertexAttributes;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.utils.Disposable;

public class LinearGradientBackground implements Disposable {
    private Matrix4 mat;
    private Mesh mesh;
    private ShaderProgram shader;
    private VertexAttributes attributes;

    public LinearGradientBackground() {
        mat = new Matrix4();
        shader = new ShaderProgram("uniform mat4 u_projTrans;\n" + "attribute vec2 a_position;\n"
                + "attribute vec4 a_color;\n" + "varying vec4 v_color;\n" + "void main() {\n"
                + " gl_Position = u_projTrans * vec4(a_position.xy, 0.0, 1.0);\n" + "     v_color = a_color;\n" + "}",

                "#ifdef GL_ES\n" + "precision mediump float;\n" + "#endif\n" + "\n" + "varying vec4 v_color;\n" + "\n"
                        + "void main() {\n" + "     gl_FragColor = v_color;\n" + "}");
        if (shader.isCompiled())
            Gdx.app.debug("log", shader.getLog());
        attributes = new VertexAttributes(new VertexAttribute(VertexAttributes.Usage.Position, 2, "a_position"),
                new VertexAttribute(VertexAttributes.Usage.Normal, 4, "a_color"));
    }

    public void setGradient(Color color1, Color color2, boolean horizontal) {
        float[] vertexData = new float[6 * 4];
        short[] indexData = new short[]{0, 1, 2, 2, 1, 3};
        vertexData[0 * 6 + 0] = -1;
        vertexData[0 * 6 + 1] = -1;
        if (horizontal) {
            vertexData[0 * 6 + 2] = color1.r;
            vertexData[0 * 6 + 3] = color1.g;
            vertexData[0 * 6 + 4] = color1.b;
            vertexData[0 * 6 + 5] = color1.a;
        } else {
            vertexData[0 * 6 + 2] = color2.r;
            vertexData[0 * 6 + 3] = color2.g;
            vertexData[0 * 6 + 4] = color2.b;
            vertexData[0 * 6 + 5] = color2.a;
        }
        vertexData[1 * 6 + 0] = 1;
        vertexData[1 * 6 + 1] = -1;
        vertexData[1 * 6 + 2] = color2.r;
        vertexData[1 * 6 + 3] = color2.g;
        vertexData[1 * 6 + 4] = color2.b;
        vertexData[1 * 6 + 5] = color2.a;

        vertexData[2 * 6 + 0] = -1;
        vertexData[2 * 6 + 1] = 1;
        vertexData[2 * 6 + 2] = color1.r;
        vertexData[2 * 6 + 3] = color1.g;
        vertexData[2 * 6 + 4] = color1.b;
        vertexData[2 * 6 + 5] = color1.a;

        vertexData[3 * 6 + 0] = 1;
        vertexData[3 * 6 + 1] = 1;
        if (horizontal) {
            vertexData[3 * 6 + 2] = color2.r;
            vertexData[3 * 6 + 3] = color2.g;
            vertexData[3 * 6 + 4] = color2.b;
            vertexData[3 * 6 + 5] = color2.a;

        } else {
            vertexData[3 * 6 + 2] = color1.r;
            vertexData[3 * 6 + 3] = color1.g;
            vertexData[3 * 6 + 4] = color1.b;
            vertexData[3 * 6 + 5] = color1.a;
        }
        if (mesh != null)
            mesh.dispose();
        mesh = new Mesh(true, 4, 6, attributes);
        mesh.setVertices(vertexData, 0, 24);
        mesh.setIndices(indexData, 0, 6);
    }

    public void draw() {
        Gdx.gl20.glDisable(GL20.GL_DEPTH_TEST);
        Gdx.gl20.glEnable(GL20.GL_BLEND);
        Gdx.gl20.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
        shader.begin();
        shader.setUniformMatrix("u_projTrans", mat);
        mesh.render(shader, GL20.GL_TRIANGLES);
        shader.end();
    }

    @Override
    public void dispose() {
        shader.dispose();
        mesh.dispose();
    }
}