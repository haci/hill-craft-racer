package com.mobaxe.hillcraftracer.helpers;

import com.badlogic.gdx.Application.ApplicationType;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator.FreeTypeFontParameter;

public class AssetsLoader {

	private static FreeTypeFontGenerator generator;
	private static FreeTypeFontParameter parameter;
	public static BitmapFont scoreText;

	private static Sound sound;
	private static Music music;
	public static Texture monsterWheel, beetleWheel, monsterTruck, beetle, gray, sand, moon, desert,
			countrySide, stageselectbg, splash, car1, car2, car3, coin, grass, cloud, gas, brake, gasOn,
			brakeOn, wheel, pickup, vehicleselectbg, playAgainBtn, homeBtn;

	public static Texture loadTexture(String filePath) {
		return new Texture(Gdx.files.internal(filePath));
	}

	public static Sound loadSound(String filePath) {
		sound = Gdx.audio.newSound(Gdx.files.internal(filePath));
		return sound;
	}

	public static Music loadMusic(String filePath) {
		music = Gdx.audio.newMusic(Gdx.files.internal(filePath));
		return music;
	}

	public static void loadTexturesOnCreate() {
		playAgainBtn = loadTexture("playagain.png");
		vehicleselectbg = loadTexture("vehiclebuttons/vehicleselectbg.png");
		pickup = loadTexture("vehicles/pickup.png");
		wheel = loadTexture("vehicles/pickupwheel.png");
		brake = loadTexture("vehicles/brake.png");
		gas = loadTexture("vehicles/gas.png");
		brakeOn = loadTexture("vehicles/brake_on.png");
		gasOn = loadTexture("vehicles/gas_on.png");
		cloud = loadTexture("cloud.png");
		grass = loadTexture("grass.png");
		coin = loadTexture("coin.png");
		car1 = loadTexture("vehiclebuttons/car1.png");
		car2 = loadTexture("vehiclebuttons/car2.png");
		car3 = loadTexture("vehiclebuttons/car3.png");
		splash = loadTexture("splash.png");
		desert = loadTexture("stagebuttons/desert.png");
		countrySide = loadTexture("stagebuttons/countryside.png");
		moon = loadTexture("stagebuttons/moon.png");
		stageselectbg = loadTexture("stagebuttons/stageselectionbg.png");
		sand = loadTexture("sand.png");
		gray = loadTexture("gray.png");
		beetle = loadTexture("vehicles/beetle.png");
		beetleWheel = loadTexture("vehicles/beetlewheel.png");
		monsterTruck = loadTexture("vehicles/monstertruck.png");
		monsterWheel = loadTexture("vehicles/monsterwheel.png");
		homeBtn = loadTexture("home.png");

		generateFont();

	}

	public static void generateFont() {
		generator = new FreeTypeFontGenerator(Gdx.files.internal("fonts/text.ttf"));
		parameter = new FreeTypeFontParameter();
		int density = 50;
		if (ApplicationType.Android == Gdx.app.getType()) {
			density = 15;
		}
		parameter.size = (int) (density * Gdx.graphics.getDensity());
		scoreText = generator.generateFont(parameter);

		generator.dispose(); // don't forget to dispose to avoid memory leaks!

	}

	public static void dispose() {
		playAgainBtn.dispose();
		vehicleselectbg.dispose();
		pickup.dispose();
		wheel.dispose();
		brake.dispose();
		brakeOn.dispose();
		gas.dispose();
		gasOn.dispose();
		cloud.dispose();
		grass.dispose();
		coin.dispose();
		car1.dispose();
		car2.dispose();
		car3.dispose();
		splash.dispose();
		desert.dispose();
		stageselectbg.dispose();
		moon.dispose();
		countrySide.dispose();
		sand.dispose();
		gray.dispose();
		homeBtn.dispose();
	}
}