package com.mobaxe.hillcraftracer.helpers;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.ChainShape;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.EdgeShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.Shape;
import com.badlogic.gdx.physics.box2d.World;
import com.mobaxe.hillcraftracer.utils.Constants;

public class Box2DFactory {

	public static Body createBody(World world, BodyType bodyType, FixtureDef fixtureDef, Vector2 position) {
		BodyDef bodyDef = new BodyDef();
		bodyDef.type = bodyType;
		bodyDef.position.set(position);

		Body body = world.createBody(bodyDef);
		body.createFixture(fixtureDef);

		fixtureDef.shape.dispose();

		return body;
	}

	public static Shape createChainShape(Vector2[] vertices) {
		ChainShape chainShape = new ChainShape();
		chainShape.createChain(vertices);
		return chainShape;
	}

	public static Shape createEdgeShape(Vector2 point1, Vector2 point2) {
		EdgeShape edgeShape = new EdgeShape();
		edgeShape.set(point1, point2);
		return edgeShape;
	}

	public static Shape createCircleShape(float radius) {
		CircleShape circleShape = new CircleShape();
		circleShape.setRadius(radius);

		return circleShape;
	}

	public static Shape createPolygonShape(Vector2[] vertices) {
		PolygonShape polygonShape = new PolygonShape();
		polygonShape.set(vertices);

		return polygonShape;
	}

	public static FixtureDef createFixture(Shape shape, float density, float friction, float restitution,
			boolean isSensor) {
		FixtureDef fixtureDef = new FixtureDef();
		fixtureDef.shape = shape;
		fixtureDef.isSensor = isSensor;
		fixtureDef.density = density;
		fixtureDef.friction = friction;
		fixtureDef.restitution = restitution;

		return fixtureDef;
	}

	public static Body createScreenBlock(World world, float x) {
		EdgeShape shape = (EdgeShape) createEdgeShape(new Vector2(x, 5), new Vector2(x, -5));
		FixtureDef fixtureDef = createFixture(shape, 1, 1f, 0.3f, false);
		fixtureDef.filter.categoryBits = Constants.LINES;
		return createBody(world, BodyType.StaticBody, fixtureDef, new Vector2(Constants.widthMeters,
				Constants.heightMeters));

	}

	public static Body createPolygon(World world, Vector2[] vertices) {

		Shape shape = createPolygonShape(vertices);
		FixtureDef fixtureDef = createFixture(shape, 1, 0.2f, 0.1f, false);
		fixtureDef.filter.categoryBits = Constants.LINES;
		return createBody(world, BodyType.StaticBody, fixtureDef, new Vector2(Constants.widthMeters,
				Constants.heightMeters));

	}

	public static Body createCloud(World world, float x, float y) {

		Vector2[] vertices = { new Vector2(1, 1.1f), new Vector2(2, 1.8f) };

		Shape shape = createChainShape(vertices);
		FixtureDef fixtureDef = createFixture(shape, 1, 0.8f, 0.3f, false);
		fixtureDef.filter.categoryBits = Constants.CLOUD;
		return createBody(world, BodyType.StaticBody, fixtureDef, new Vector2(Constants.widthMeters + x,
				Constants.heightMeters + y));

	}

	public static Body createCoins(World world, float x, float y) {

		Shape shape = createCircleShape(0.2f);
		FixtureDef fixtureDef = createFixture(shape, 1, 0.8f, 0.3f, false);
		return createBody(world, BodyType.StaticBody, fixtureDef, new Vector2(Constants.widthMeters + x,
				Constants.heightMeters + y));

	}

	public static Body createEarth(World world, float x, float y) {

		Shape shape = createCircleShape(0.2f);
		FixtureDef fixtureDef = createFixture(shape, 1, 0.8f, 0.3f, false);
		return createBody(world, BodyType.KinematicBody, fixtureDef, new Vector2(Constants.widthMeters + x,
				Constants.heightMeters + y));

	}
}
