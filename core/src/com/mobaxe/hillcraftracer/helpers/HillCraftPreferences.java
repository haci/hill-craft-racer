package com.mobaxe.hillcraftracer.helpers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;

public class HillCraftPreferences {

	private String TOTAL_BALANCE = "totalbalance";
	private String V2LOCK = "v2lock";
	private String V3LOCK = "v3lock";
	private String S2LOCK = "s2lock";
	private String S3LOCK = "s3lock";

	private static Preferences prefs = Gdx.app.getPreferences("hcr");

	// V2 LOCK
	public void saveV2Lock(boolean lock) {
		prefs.putBoolean(V2LOCK, lock);
		prefs.flush();
	}

	public boolean getV2Lock() {
		return prefs.getBoolean(V2LOCK);
	}

	// V3 LOCK
	public void saveV3Lock(boolean lock) {
		prefs.putBoolean(V3LOCK, lock);
		prefs.flush();
	}

	public boolean getV3Lock() {
		return prefs.getBoolean(V3LOCK);
	}

	// S2 LOCK
	public void saveS2Lock(boolean lock) {
		prefs.putBoolean(S2LOCK, lock);
		prefs.flush();
	}

	public boolean getS2Lock() {
		return prefs.getBoolean(S2LOCK);
	}

	// V3 LOCK
	public void saveS3Lock(boolean lock) {
		prefs.putBoolean(S3LOCK, lock);
		prefs.flush();
	}

	public boolean getS3Lock() {
		return prefs.getBoolean(S3LOCK);
	}

	// TOTAL AMOUNT
	public void saveTotalBalance(int amount) {
		prefs.putInteger(TOTAL_BALANCE, amount);
		prefs.flush();
	}

	public int getTotalBalance() {
		return prefs.getInteger(TOTAL_BALANCE);
	}

	public void clearPrefs() {
		prefs.clear();
	}

}
