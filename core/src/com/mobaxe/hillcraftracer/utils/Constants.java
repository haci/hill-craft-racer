package com.mobaxe.hillcraftracer.utils;

import com.badlogic.gdx.Gdx;

public class Constants {

	public static final int VEHICLE2_COST = 3000;
	public static final int VEHICLE3_COST = 10000;
	public static final int STAGE2_COST = 1000;
	public static final int STAGE3_COST = 3000;

	public static final String VEHICLEBUTTON1 = "vehicle1";
	public static final String VEHICLEBUTTON2 = "vehicle2";
	public static final String VEHICLEBUTTON3 = "vehicle3";
	public static final String STAGEBUTTON1 = "stage1";
	public static final String STAGEBUTTON2 = "stage2";
	public static final String STAGEBUTTON3 = "stage3";

	public static final int INTERSTITIAL_FREQ = 1;

	public static final short LINES = 0x0002;
	public static final short BUTTON = 0x0000;
	public static final short CLOUD = 0x0000;

	private static final float offset = 0.5f;
	private static final float width = 20;

	public static float realWidth = Gdx.graphics.getWidth();
	public static float realHeight = Gdx.graphics.getHeight();

	public static final float virtualWidth = 800;
	public static final float virtualHeight = 480;

	private static final float height = 20 * (virtualHeight / virtualWidth);

	public static final float widthMeters = width / 2 - offset;
	public static final float heightMeters = height / 2 - offset;

}
